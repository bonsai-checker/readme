This Bitbucket page contains three repositories, each of which corresponds to a language studied in our POPL'18 paper. To run any of the three, you first need

1. An installation of Racket (http://racket-lang.org), with the `racket` binary in your PATH so that you can access the REPL from the command line.
2. An installation of Rosette (http://emina.github.io/rosette/). If you have already installed Racket, then you can install Rosette with `$ raco pkg install rosette`; you can then run `$ racket -I rosette` to see if it is installed. Further instructions are on the webpage.

The contents of the three repositories are listed below — simply download/clone them to obtain a copy.

* stlc-benchmark contains the lambda-calculus benchmark described in Section 6.1 of the paper. The file `stlc.rkt` contains an implementation of the original language; each file in the `patch/` directory is a diff that introduces a bug. The script `run-tests.sh` will apply each patch to `stlc.rkt` and run Bonsai to find a counterexample, collecting timing statistics along the way.
* let-poly implements the "classic let+references" bug mentioned at the end of Section 6.1 of the paper. To run the tool, simply execute `$ racket let-poly.rkt`. It takes around 20 minutes on my machine. The `let-poly.rkt` source is also heavily commented, serving as an introductory example for using the `bonsai.rkt` library.
* Finally, dot implements the Dependent Object Types language described in Section 5.3. Running nanodot.rkt exposes the first bug ("Disjoint domains in intersection types.") while running nanoscala.rkt exposes the second bug ("Collapsing the subtype hierarchy using bad bounds (SI-9633).").